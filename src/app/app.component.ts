import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';

import * as firebase from 'firebase';
import { FIREBASE_CONFIG } from './firebase.credentials';
//import { initializeApp } from "firebase";

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { initializeApp } from 'firebase';
import { MenuPage } from '../pages/menu/menu';
import { AggrementPage } from '../pages/aggrement/aggrement';
import { FicheClientPage } from '../pages/fiche-client/fiche-client';
import { FraiseEventPage } from '../pages/sareqdatabase/fraise-event/fraise-event';
import { SardisPage } from '../pages/sareqdatabase/sardis/sardis';
import { SareqbtpPage } from '../pages/sareqdatabase/sareqbtp/sareqbtp';
import { SareqTransPage } from '../pages/sareqdatabase/sareq-trans/sareq-trans';
import { FactureDefinitivePage } from '../pages/facture-definitive/facture-definitive';
import { FactureProformatPage } from '../pages/facture-proformat/facture-proformat';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
  rootPage:any;




  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private menuCtrl: MenuController,) {
    initializeApp(FIREBASE_CONFIG);
  
   
var that = this;

    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        that.menuCtrl.enable(true);
        that.rootPage = HomePage;
        // ...
      } else {
        // User is signed out.
        // ...
        that.rootPage = LoginPage;
      }
    });

    

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  accueil(){
    this.navCtrl.setRoot(HomePage);
  }  
  prospect(){
    this.navCtrl.setRoot(FicheClientPage);
  }  
  aggrement(){
    this.navCtrl.setRoot(AggrementPage);
  }  
  fraisevent(){
    this.navCtrl.setRoot(FraiseEventPage);
  }  
  sardis(){
    this.navCtrl.setRoot(SardisPage);
  }  
  sareqbtp(){
    this.navCtrl.setRoot(SareqbtpPage);
  }  
  sareqtrans(){
    this.navCtrl.setRoot(SareqTransPage);
  }  
  factureD(){
    this.navCtrl.setRoot(FactureDefinitivePage);
  }  
  factureP(){
    this.navCtrl.setRoot(FactureProformatPage);
  }  


}