import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/Auth';
import { AngularFireModule } from 'angularfire2';
import { FIREBASE_CONFIG } from './firebase.credentials';
import { UsersserviceProvider } from '../providers/usersservice/usersservice';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { HttpModule } from '@angular/http';
import { MenuPage } from '../pages/menu/menu';
import { AggrementPage } from '../pages/aggrement/aggrement';
import { FicheClientPage } from '../pages/fiche-client/fiche-client';
import { FraiseEventPage } from '../pages/sareqdatabase/fraise-event/fraise-event';
import { SardisPage } from '../pages/sareqdatabase/sardis/sardis';
import { SareqbtpPage } from '../pages/sareqdatabase/sareqbtp/sareqbtp';
import { SareqTransPage } from '../pages/sareqdatabase/sareq-trans/sareq-trans';
import { FactureProformatPage } from '../pages/facture-proformat/facture-proformat';
import { FactureDefinitivePage } from '../pages/facture-definitive/facture-definitive';

import { AngularFireStorageModule } from '@angular/fire/storage';
import { FraiseEventService } from '../providers/fraise-event/fraise-event.service';
import { DetailFraiseEventPage } from '../pages/detail-fraise-event/detail-fraise-event';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    MenuPage,
    AggrementPage,
    FicheClientPage,
    FraiseEventPage,
    SardisPage,
    SareqbtpPage,
    SareqTransPage,
    FactureProformatPage,
    FactureDefinitivePage,
    DetailFraiseEventPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    MenuPage,
    AggrementPage,
    FicheClientPage,
    FraiseEventPage,
    SardisPage,
    SareqbtpPage,
    SareqTransPage,
    FactureDefinitivePage,
    FactureProformatPage,
    DetailFraiseEventPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsersserviceProvider,
    FraiseEventService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
