import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { Item } from "../../pages/models/item/item.model";

@Injectable()

export class FraiseEventService {

    private shoppingListRef = this.db.list<Item>('fraise-event')

    constructor(private db: AngularFireDatabase){

    }

    addItem(item: Item){
        return this.shoppingListRef.push(item);
    }

    getShoppingList(){
        return this.shoppingListRef;
    }
    
}