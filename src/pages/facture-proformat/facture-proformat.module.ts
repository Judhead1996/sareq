import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FactureProformatPage } from './facture-proformat';

@NgModule({
  declarations: [
    FactureProformatPage,
  ],
  imports: [
    IonicPageModule.forChild(FactureProformatPage),
  ],
})
export class FactureProformatPageModule {}
