import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SareqbtpPage } from './sareqbtp';

@NgModule({
  declarations: [
    SareqbtpPage,
  ],
  imports: [
    IonicPageModule.forChild(SareqbtpPage),
  ],
})
export class SareqbtpPageModule {}
