import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SardisPage } from './sardis';

@NgModule({
  declarations: [
    SardisPage,
  ],
  imports: [
    IonicPageModule.forChild(SardisPage),
  ],
})
export class SardisPageModule {}
