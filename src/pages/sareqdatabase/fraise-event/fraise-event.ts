import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FraiseEventService } from '../../../providers/fraise-event/fraise-event.service';
import { Item } from '../../models/item/item.model';
import { AngularFireDatabase } from "@angular/fire/database";
import { SardisPage } from '../sardis/sardis';
import { DetailFraiseEventPage } from '../../detail-fraise-event/detail-fraise-event';

/**
 * Generated class for the FraiseEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fraise-event',
  templateUrl: 'fraise-event.html',
})
export class FraiseEventPage {

  item : Item = {
    contact: '',
    delai: '',
    validite: '',
    contactsareq: ''
  }

  // shoppingListRef = this.db.list<Item>('fraise-event')

  result= [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
     private shopping: FraiseEventService, public loadingCtrl: LoadingController,
      private alertCtrl: AlertController,
      private db: AngularFireDatabase) {
        this.getItem();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FraiseEventPage');
  }

  public test(event ,detail ){
    this.navCtrl.push(DetailFraiseEventPage,{
      detail:detail
    });
    } 

  addItem(item: Item){
    var that = this;

    var loader = this.loadingCtrl.create({
      content: "Please Wait..."
    });
    loader.present();
    this.shopping.addItem(item).then(ref => {

      console.log(ref.key);
      loader.dismiss();
      that.navCtrl.setRoot(FraiseEventPage);
    })
   }
   
    getItem(){
     // this.db.object('/fraise-event/').valueChanges().subscribe( x => console.log("Fast life", Object.keys(x)))
      this.db.object('/fraise-event/').valueChanges().subscribe(data => {
         
         let arr = Object.keys(data);
         this.result = [];
          for (var i = 0; i < arr.length; i++) {
             const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]); 
             this.result.push(object2); 
             console.log("Fast life", this.result);
            }
    })
   }

   presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Client',
      inputs: [
        {
          name: 'contact',
          placeholder: 'Contact Client'
        },
        {
          name: 'delai',
          placeholder: 'Délai de livraison',
        },
        {
          name: 'validite',
          placeholder: 'Validité offre'
        },
        {
          name: 'contactsareq',
          placeholder: 'Contact Sareq',
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Envoyer',
          handler: data => {
            var that = this;
            var loader = this.loadingCtrl.create({
              content: "Please Wait...",
              duration: 2000
            });
            loader.present();
            if (data.contact, data.delai, data.validite, data.contactsareq) {
              this.shopping.addItem(data).then(ref => {

                console.log(ref.key); 
                loader.dismiss();
                that.navCtrl.setRoot(FraiseEventPage);
              })
            } else {
              // invalid login
              return false;
            } 
          }
        }
      ]
    });
    alert.present();
  }

}
