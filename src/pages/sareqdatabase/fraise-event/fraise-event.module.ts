import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FraiseEventPage } from './fraise-event';

@NgModule({
  declarations: [
    FraiseEventPage,
  ],
  imports: [
    IonicPageModule.forChild(FraiseEventPage),
  ],
})
export class FraiseEventPageModule {}
