import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AngularFireDatabase } from "@angular/fire/database";


/**
 * Generated class for the SareqTransPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
export interface Item {
  key?: string;
  contact: string;
  delai: string;
  validite: string;
  contactsareq: string;
}

@IonicPage()
@Component({
  selector: 'page-sareq-trans',
  templateUrl: 'sareq-trans.html',
})
export class SareqTransPage {

  item : Item = {
    contact: '',
    delai: '',
    validite: '',
    contactsareq: ''
  }

  result= [];

  private sardisAjout = this.db.list<Item>('sareq-trans')

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private db: AngularFireDatabase,
    public loadingCtrl: LoadingController,
      private alertCtrl: AlertController) {
        this.getItem();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SareqTransPage');
  }

  addItem(item: Item){
    return this.sardisAjout.push(item);
  }

  ajoutPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Client',
      inputs: [
        {
          name: 'contact',
          placeholder: 'Contact Client'
        },
        {
          name: 'delai',
          placeholder: 'Délai de livraison',
        },
        {
          name: 'validite',
          placeholder: 'Validité offre'
        },
        {
          name: 'contactsareq',
          placeholder: 'Contact Sareq',
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Envoyer',
          handler: data => {
            var that = this;
            var loader = this.loadingCtrl.create({
              content: "Please Wait...",
              duration: 2000
            });
            loader.present();
            if (data.contact, data.delai, data.validite, data.contactsareq) {
              this.addItem(data).then(ref => {

                console.log(ref.key); 
                loader.dismiss();
                that.navCtrl.setRoot(SareqTransPage);
              })
            } else {
              // invalid login
              return false;
            } 
          }
        }
      ]
    });
    alert.present();
  }

  getItem(){
    
     this.db.object('/sareq-trans/').valueChanges().subscribe(data => {
        
        let arr = Object.keys(data);
        this.result = [];
         for (var i = 0; i < arr.length; i++) {
            const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
            this.result.push(object2); 
            console.log("Fast life", this.result);
           }
   })
 }

}
