import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SareqTransPage } from './sareq-trans';

@NgModule({
  declarations: [
    SareqTransPage,
  ],
  imports: [
    IonicPageModule.forChild(SareqTransPage),
  ],
})
export class SareqTransPageModule {}
