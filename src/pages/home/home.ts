import { Component } from '@angular/core';
import { NavController, Nav } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import * as firebase from "firebase";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

 

	private nav: Nav;

  constructor(public navCtrl: NavController, nav: Nav) {
    this.nav = nav;
		

  }

  public navigateTo(tile) {
		this.nav.setRoot(tile.component);
  }

  

  logOut(){
    return firebase.auth().signOut();
  }  
  
  

}
