import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';

/**
 * Generated class for the DetailFraiseEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-fraise-event',
  templateUrl: 'detail-fraise-event.html',
})
export class DetailFraiseEventPage {

  value: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private db: AngularFireDatabase) {
    this.value = navParams.get('detail');    
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailFraiseEventPage');
  }

 

}
