import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailFraiseEventPage } from './detail-fraise-event';

@NgModule({
  declarations: [
    DetailFraiseEventPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailFraiseEventPage),
  ],
})
export class DetailFraiseEventPageModule {}
