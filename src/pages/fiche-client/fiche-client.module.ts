import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FicheClientPage } from './fiche-client';

@NgModule({
  declarations: [
    FicheClientPage,
  ],
  imports: [
    IonicPageModule.forChild(FicheClientPage),
  ],
})
export class FicheClientPageModule {}
