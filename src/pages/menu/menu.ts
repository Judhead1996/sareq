import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
import * as firebase from "firebase";
import 'rxjs/Rx';
import { Observable } from "rxjs/Observable";
import { UsersserviceProvider } from '../../providers/usersservice/usersservice';
import { AngularFireAuth } from 'angularfire2/Auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { AggrementPage } from '../aggrement/aggrement';
/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

 
  constructor(public navCtrl: NavController, public navParams: NavParams,public usersserviceProvider:UsersserviceProvider,
    private afAuth: AngularFireAuth, private db: AngularFireDatabase, public toast: ToastController) {

  


  }

 
  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

}
