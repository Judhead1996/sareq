import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AggrementPage } from './aggrement';

@NgModule({
  declarations: [
    AggrementPage,
  ],
  imports: [
    IonicPageModule.forChild(AggrementPage),
  ],
})
export class AggrementPageModule {}
