import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, MenuController } from 'ionic-angular';
import { AngularFireDatabase } from "@angular/fire/database";

import * as firebase from 'firebase';
import { HomePage } from "../home/home";
import { SignupPage } from '../signup/signup';
import { UsersserviceProvider } from '../../providers/usersservice/usersservice';
import 'rxjs/Rx';
import { AngularFireDatabaseModule } from 'angularfire2/database';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [UsersserviceProvider]
})
export class LoginPage {

  public email: string;
  public password: string;
  result= [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, 
    public toastCtrl: ToastController,public usersService: UsersserviceProvider, private menuCtrl: MenuController,
    private db: AngularFireDatabase ) {
      this.menuCtrl.enable(false);
  }
 

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  submitLogin(){

    var that = this;

    var loader = this.loadingCtrl.create({
      content: "Please Wait..."
    });
    loader.present();

    this.usersService.loginUserService(this.email, this.password).then(authData => {
      //successful
      loader.dismiss();

      console.log("Success")
      that.navCtrl.setRoot(HomePage);

    },error => {
      loader.dismiss();
      //Unable to log in
        let toast = this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'top'
        });
        toast.present();

      that.password="" //Empty the password field  

    });


  }

  

  forgotPassword(){

  }

  redirectToSignup(){
    this.navCtrl.push(SignupPage);
  }


}
