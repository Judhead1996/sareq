import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FactureDefinitivePage } from './facture-definitive';

@NgModule({
  declarations: [
    FactureDefinitivePage,
  ],
  imports: [
    IonicPageModule.forChild(FactureDefinitivePage),
  ],
})
export class FactureDefinitivePageModule {}
